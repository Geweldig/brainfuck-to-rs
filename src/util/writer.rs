use crate::util::program::Program;
use crate::util::Instruction;
use std::cmp::Ordering;
use std::fs::File;
use std::io::Write;
use std::process::Command;

pub struct Writer {
    indent: usize,
    file: File,
    file_name: String,
}

impl Writer {
    pub fn new(file_name: &str) -> Writer {
        let file_name = format!("{}.rs", file_name);
        Writer {
            indent: 0,
            file: std::fs::File::create(&file_name).expect("Can not create output file!"),
            file_name,
        }
    }

    fn write_line(&mut self, line: &str) {
        if line.ends_with('}') {
            self.indent -= 1;
        }
        self.file
            .write_all(
                vec![' '; self.indent * 4]
                    .iter()
                    .collect::<String>()
                    .as_bytes(),
            )
            .expect("Failed to write!");
        self.file
            .write_all(line.as_bytes())
            .expect("Failed to write!");
        self.file.write_all(b"\n").expect("Failed to write!");
        if line.ends_with('{') {
            self.indent += 1;
        }
    }

    pub fn write(&mut self, program: Program) {
        if program.does_read() {
            self.write_line("use std::env;");
            self.write_line("");
        }
        self.write_line("fn main() {");
        if program.has_instructions() {
            if program.does_read() {
                self.write_line("let input = env::args().nth(1).unwrap_or_else(String::new);");
                self.write_line("let mut input = input.chars().map(|x| x as u8);");
            }
            self.write_line("let mut tape: [u8; 300000] = [0u8; 300000];");
            self.write_line("let mut ptr = 0;");
            program.get_instructions().iter().for_each(|x| match *x {
                Instruction::IncPtr(val) => match val.cmp(&0) {
                    Ordering::Greater => self.write_line(format!("ptr += {val};").as_str()),
                    Ordering::Less => self.write_line(format!("ptr -= {};", -val).as_str()),
                    _ => (),
                },
                Instruction::Inc(val) => match val.cmp(&0) {
                    Ordering::Greater => self.write_line(format!("tape[ptr] += {val};").as_str()),
                    Ordering::Less => self.write_line(format!("tape[ptr] -= {};", -val).as_str()),
                    _ => (),
                },
                Instruction::Write => self.write_line("print!(\"{}\", tape[ptr] as char);"),
                Instruction::Read => {
                    self.write_line("tape[ptr] = input.next().unwrap_or_default();")
                }
                Instruction::StartLoop => self.write_line("while tape[ptr] != 0 {"),
                Instruction::EndLoop => self.write_line("}"),
            });
        }
        self.write_line("}");
    }

    pub fn compile(self) {
        Command::new("rustc")
            .args(["-C", "opt-level=3", &self.file_name])
            .output()
            .expect("Unable to compile file!");
    }
}
