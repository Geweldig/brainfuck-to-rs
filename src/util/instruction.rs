#[derive(Eq, PartialEq, Clone, Copy)]
pub enum Instruction {
    IncPtr(isize),
    Inc(isize),
    Write,
    Read,
    StartLoop,
    EndLoop,
}
