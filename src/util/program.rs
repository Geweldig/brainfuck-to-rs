use crate::util::Instruction;

pub struct Program {
    instructions: Vec<Instruction>,
}

impl Program {
    pub fn compile(program: Vec<u8>, optimise: bool) -> Program {
        let mut compiled = Program {
            instructions: vec![],
        };
        program.iter().for_each(|x| match x {
            b'>' => compiled.push(Instruction::IncPtr(1)),
            b'<' => compiled.push(Instruction::IncPtr(-1)),
            b'+' => compiled.push(Instruction::Inc(1)),
            b'-' => compiled.push(Instruction::Inc(-1)),
            b'.' => compiled.push(Instruction::Write),
            b',' => compiled.push(Instruction::Read),
            b'[' => compiled.push(Instruction::StartLoop),
            b']' => compiled.push(Instruction::EndLoop),
            _ => (),
        });
        if optimise {
            compiled.optimise_program();
        }
        compiled
    }

    fn optimise_program(&mut self) {
        let mut optimised = vec![];

        for instruction in self.instructions.iter() {
            match instruction {
                curr_inst @ Instruction::IncPtr(x) => match optimised.pop() {
                    Some(Instruction::IncPtr(y)) => optimised.push(Instruction::IncPtr(x + y)),
                    Some(i) => {
                        optimised.push(i);
                        optimised.push(*curr_inst);
                    }
                    None => optimised.push(*curr_inst),
                },
                curr_inst @ Instruction::Inc(x) => match optimised.pop() {
                    Some(Instruction::Inc(y)) => optimised.push(Instruction::Inc(x + y)),
                    Some(i) => {
                        optimised.push(i);
                        optimised.push(*curr_inst);
                    }
                    None => optimised.push(*curr_inst),
                },
                _ => optimised.push(*instruction),
            }
        }

        self.instructions = optimised
            .iter()
            .filter(|x| **x != Instruction::IncPtr(0) && **x != Instruction::Inc(0))
            .copied()
            .collect();
    }

    fn push(&mut self, instruction: Instruction) {
        self.instructions.push(instruction);
    }

    pub fn does_read(&self) -> bool {
        self.instructions.contains(&Instruction::Read)
    }

    pub fn get_instructions(&self) -> &Vec<Instruction> {
        &self.instructions
    }

    pub fn has_instructions(&self) -> bool {
        !self.instructions.is_empty()
    }
}
