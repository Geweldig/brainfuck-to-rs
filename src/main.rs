mod util;

use crate::util::Program;
use crate::util::Writer;
use std::path::Path;
use std::{env, fs};

fn main() {
    let mut args = env::args().skip(1);
    let file = args.next().expect("Input file not provided!");
    let program = Program::compile(fs::read(&file).unwrap(), true);
    let file_name = Path::new(&file)
        .file_stem()
        .expect("Input file does not exist!")
        .to_str()
        .expect("Input file name is not valid!");
    let mut writer = Writer::new(file_name);
    writer.write(program);
    writer.compile();
}
